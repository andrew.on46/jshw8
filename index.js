const input = document.createElement("input");

document.body.append(input);
input.setAttribute("placeholder", "Price");

input.addEventListener("focus", focusinBorder);
input.addEventListener("focusout", focusoutBorder);

function focusinBorder() {
  input.style.cssText = `
  outline: none;
  border: 2px solid green;
  border-radius: 2px;
  `;
}

function focusoutBorder() {
  input.style.cssText = "outline: black";
  if (
    Number.isNaN(+input.value) ||
    input.value.trim() === 0 ||
    input.value <= 0
  ) {
    input.style.cssText = `
    outline: none;
    border: 2px solid red;
    border-radius: 2px;
    `;
    alert("Please enter correct price");
  } else {
    const span = document.createElement("span");
    const deleteBtn = createDeleteButton();

    span.innerText = `Текущая цена: ${input.value}`;
    span.appendChild(deleteBtn);

    input.style["color"] = "green";

    document.body.append(span);
  }
}

function createDeleteButton() {
  const btn = document.createElement("button");
  btn.innerText = "X";
  btn.onclick = (e) => {
    e.target.closest("span").remove();
    input.value = "";
  };
  return btn;
}
